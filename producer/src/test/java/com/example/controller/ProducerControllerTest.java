package com.example.controller;

import com.example.service.ProducerService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

@SpringBootTest
@AutoConfigureMockMvc
class ProducerControllerTest {

    @MockBean
    ProducerService producerService;

    @Autowired
    MockMvc mockMvc;

    @Test
    void triggerConsumerHappyPath() throws Exception {
        //given
        var callTimes = 3;
        doNothing().when(producerService).callConsumerEndpoint(callTimes);

        //when
        mockMvc.perform(post("/producer?callTimes=" + callTimes)
                .contentType(MediaType.APPLICATION_JSON)
                .content(Mockito.anyString()));

        //then
        verify(producerService).callConsumerEndpoint(Mockito.any());
    }
}