package com.example.service.impl;

import com.example.config.ConsumerConfig;
import com.example.service.ProducerService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.web.client.RestTemplate;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest
public class ProducerServiceTest {

    @MockBean
    RestTemplate restTemplate;

    @MockBean
    ConsumerConfig config;

    @Autowired
    ProducerService producerService;

    @Test
    void callConsumerEndpointHappyPath() {
        //given
        var callTimes = 3;
        var CONSUMER_PATH = "/consumer";

        //when
        producerService.callConsumerEndpoint(callTimes);

        //then
        verify(config, times(callTimes)).getHost();
        verify(restTemplate, times(callTimes))
                .postForEntity(config.getHost() + CONSUMER_PATH, null, String.class);
    }
}
