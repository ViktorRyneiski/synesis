package com.example.controller;

import com.example.service.ConsumerService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequiredArgsConstructor
@RequestMapping(path = "/consumer")
public class ConsumeController {

    private final ConsumerService service;

    @GetMapping
    public String getJmxInfo() {
        return service.getJmxStatistics();
    }

    @PostMapping
    public void saveProducerInfo(HttpServletRequest request) {
        service.saveProducerInfo(request);
    }
}
