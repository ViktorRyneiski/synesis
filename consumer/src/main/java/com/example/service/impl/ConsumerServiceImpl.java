package com.example.service.impl;

import com.example.jmx.ConsumerServiceMBeam;
import com.example.service.ConsumerService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
@Slf4j
@RequiredArgsConstructor
public class ConsumerServiceImpl implements ConsumerService {

    private final ConsumerServiceMBeam consumerServiceMBeam;

    @Override
    public void saveProducerInfo(HttpServletRequest request) {
        var remoteAddr = request.getRemoteAddr();

        consumerServiceMBeam.saveJmxStatistics(remoteAddr);

        log.info("Requesting server address: {}", remoteAddr);
    }

    @Override
    public String getJmxStatistics() {
        return consumerServiceMBeam.getJmxStatistics().toString();
    }
}
