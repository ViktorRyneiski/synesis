package com.example.service;

import javax.servlet.http.HttpServletRequest;

public interface ConsumerService {

    void saveProducerInfo(HttpServletRequest request);

    String getJmxStatistics();

}
