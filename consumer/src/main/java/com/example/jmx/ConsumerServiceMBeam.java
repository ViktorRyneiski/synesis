package com.example.jmx;

import org.springframework.jmx.export.annotation.ManagedAttribute;
import org.springframework.jmx.export.annotation.ManagedResource;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

@Component
@ManagedResource(description = "Info about call method")
public class ConsumerServiceMBeam {

    private final Map<String, Integer> requestCountMap = new HashMap<>();

    @ManagedAttribute(description = "Return info about who and what time called POST method")
    public Map<String, Integer> getJmxStatistics() {
        return requestCountMap;
    }

    public void saveJmxStatistics(String remoteAddr) {
        this.requestCountMap.put(remoteAddr, requestCountMap.getOrDefault(remoteAddr, 0) + 1);
    }
}
