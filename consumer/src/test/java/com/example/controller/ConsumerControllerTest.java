package com.example.controller;

import com.example.service.ConsumerService;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class ConsumerControllerTest {

    @MockBean
    ConsumerService consumerService;

    @Autowired
    MockMvc mockMvc;

    @Test
    void saveProducerInfoHappyPath() throws Exception {
        //given
        doNothing().when(consumerService).saveProducerInfo(Mockito.any());

        //when
        mockMvc.perform(post("/consumer")
                .contentType(MediaType.APPLICATION_JSON)
                .content(Mockito.anyString()));

        //then
        verify(consumerService).saveProducerInfo(Mockito.any());
    }

    @Test
    void getJmxInfoHappyPath() throws Exception {
        //given
        Map<String, Integer> jmxStatistics = new HashMap<>();
        jmxStatistics.put("127.1.1.1", 8);

        when(consumerService.getJmxStatistics()).thenReturn(jmxStatistics.toString());

        //when
        var result = mockMvc.perform(get("/consumer"))
                .andExpect(status().isOk())
                .andReturn();

        var response = result.getResponse().getContentAsString();

        //then
        verify(consumerService).getJmxStatistics();
        assertEquals(response, jmxStatistics.toString());
    }
}