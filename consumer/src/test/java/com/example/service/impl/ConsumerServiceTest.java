package com.example.service.impl;

import com.example.jmx.ConsumerServiceMBeam;
import com.example.service.ConsumerService;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
class ConsumerServiceTest {

    @MockBean
    ConsumerServiceMBeam consumerServiceMBeam;

    @Autowired
    ConsumerService consumerService;

    @MockBean
    HttpServletRequest request;

    @Test
    void saveProducerInfoHappyPath() {
        //given

        doNothing().when(consumerServiceMBeam).saveJmxStatistics(Mockito.any());

        //when
        consumerService.saveProducerInfo(request);

        //then
        verify(consumerServiceMBeam).saveJmxStatistics(Mockito.any());
    }

    @Test
    void getJmxStatisticsHappyPath() {
        //given
        Map<String, Integer> expectedJmxStatistics = new HashMap<>();
        expectedJmxStatistics.put("127.1.1.1", 8);

        when(consumerServiceMBeam.getJmxStatistics()).thenReturn(expectedJmxStatistics);

        //when
        String returnedJmxStatistics = consumerService.getJmxStatistics();

        //then
        verify(consumerServiceMBeam).getJmxStatistics();
        assertEquals(expectedJmxStatistics.toString(), returnedJmxStatistics);
    }
}